package org.teamseas.minecraft.bossbar;

import static org.junit.Assert.assertFalse;
import static org.junit.Assert.assertTrue;

import java.util.UUID;
import org.bukkit.Bukkit;
import org.bukkit.entity.Player;
import org.junit.After;
import org.junit.Before;
import org.junit.Test;
import org.mockito.MockedStatic;
import org.mockito.Mockito;
import redis.clients.jedis.Jedis;
import redis.embedded.RedisServer;

public class JedisBossbarRepositoryTest {

  private MockedStatic<Bukkit> bukkit;
  private RedisServer server;
  private Jedis jedis;
  private BossbarRepository repository;

  @Before
  public void setUp() throws Exception {
    bukkit = Mockito.mockStatic(Bukkit.class);
    server = new RedisServer();
    server.start();
    jedis = new Jedis();
    repository = new JedisBossbarRepository(jedis);
  }

  @After
  public void tearDown() throws Exception {
    server.stop();
    bukkit.close();
  }

  @Test
  public void initialVisibility() {
    Player player = player("1-2-3-4-5");
    assertFalse(repository.isVisible(player));
  }

  @Test
  public void storesVisibility() {
    Player player = player("1-2-3-4-5");
    repository.setVisible(player, true);
    assertTrue(repository.isVisible(player));
  }

  private Player player(String id) {
    Player player = Mockito.mock(Player.class);
    UUID uuid = UUID.fromString(id);
    Mockito.when(player.getUniqueId()).thenReturn(uuid);
    bukkit.when(() -> Bukkit.getOfflinePlayer(uuid)).thenReturn(player);
    return player;
  }
}
