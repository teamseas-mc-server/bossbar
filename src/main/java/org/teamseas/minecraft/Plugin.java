package org.teamseas.minecraft;

import org.bukkit.command.PluginCommand;
import org.bukkit.plugin.java.JavaPlugin;
import org.teamseas.minecraft.bossbar.Bossbar;
import org.teamseas.minecraft.bossbar.BossbarCommand;
import org.teamseas.minecraft.bossbar.BossbarCommandTabCompleter;
import org.teamseas.minecraft.bossbar.BossbarRepository;
import org.teamseas.minecraft.bossbar.BossbarService;
import org.teamseas.minecraft.bossbar.JedisBossbarRepository;
import redis.clients.jedis.Jedis;

/**
 * The main class of the plugin.
 */
public class Plugin extends JavaPlugin {

  private Jedis jedis;

  @Override
  public void onEnable() {
    String host = System.getenv().getOrDefault("REDIS_HOST", "localhost");
    String port = System.getenv().getOrDefault("REDIS_PORT", "6379");
    jedis = new Jedis(host, Integer.parseInt(port));
    
    BossbarRepository repository = new JedisBossbarRepository(jedis);
    BossbarService service = new BossbarService();
    Bossbar bossbar = new Bossbar(repository, service);

    getServer().getPluginManager().registerEvents(bossbar.getListener(), this);
    getServer().getScheduler().runTaskTimer(this, bossbar.getTimerTask(), 0, 30 * 20);

    PluginCommand bossbarCommand = getCommand("bossbar");
    if (bossbarCommand != null) {
      bossbarCommand.setExecutor(new BossbarCommand(repository, bossbar));
      bossbarCommand.setTabCompleter(new BossbarCommandTabCompleter());
    }
  }

  @Override
  public void onDisable() {
    if (jedis != null) {
      jedis.close();
      jedis = null;
    }
  }
}
