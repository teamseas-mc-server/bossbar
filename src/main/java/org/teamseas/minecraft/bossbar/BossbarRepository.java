package org.teamseas.minecraft.bossbar;

import org.bukkit.OfflinePlayer;

/**
 * Data repository to store the player specific configuration of it's bossbar.
 */
public interface BossbarRepository {

  void setVisible(OfflinePlayer player, boolean isVisible);

  boolean isVisible(OfflinePlayer player);
}
