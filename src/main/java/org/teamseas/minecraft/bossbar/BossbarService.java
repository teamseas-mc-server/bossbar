package org.teamseas.minecraft.bossbar;

import java.util.Locale;
import java.util.Optional;
import kong.unirest.HttpResponse;
import kong.unirest.JsonNode;
import kong.unirest.Unirest;
import lombok.AllArgsConstructor;
import lombok.Getter;
import org.bukkit.ChatColor;
import org.teamseas.minecraft.util.IntegerPrinter;

/**
 * Helper class to generate new text for bossbar.
 */
public class BossbarService {

  private static final IntegerPrinter PRINTER = new IntegerPrinter();
  private static final String ENDPOINT = "https://assets01.teamassets.net/json/donation_total.json";

  /**
   * Helper class to wrap information.
   */
  @Getter
  @AllArgsConstructor
  public static class Information {
    public float progress;
    public String text;
  }

  /**
   * Grabs new statistics for bossbar and returns new information.
   */
  public Optional<Information> getInformation() {
    HttpResponse<JsonNode> response = Unirest.get(ENDPOINT).asJson();
    if (response.getStatus() == 200) {
      Integer count = Integer.parseInt(response.getBody().getObject().getString("count"));

      float progress = count.floatValue() / 30_000_000;

      String formattedCount = PRINTER.print(count, Locale.US);
      String text = color(String.format("&6%s &b/ &630M &b(&6%.1f%%&b) &d- teamseas.org",
          formattedCount, progress * 100));

      return Optional.of(new Information(progress, text));
    }

    return Optional.empty();
  }

  private static String color(String text) {
    return ChatColor.translateAlternateColorCodes('&', text);
  }
}
