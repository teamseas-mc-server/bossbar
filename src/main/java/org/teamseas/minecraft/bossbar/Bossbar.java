package org.teamseas.minecraft.bossbar;

import java.util.Optional;
import org.bukkit.Bukkit;
import org.bukkit.boss.BarColor;
import org.bukkit.boss.BarStyle;
import org.bukkit.boss.BossBar;
import org.bukkit.entity.Player;
import org.bukkit.event.EventHandler;
import org.bukkit.event.Listener;
import org.bukkit.event.player.PlayerJoinEvent;
import org.bukkit.event.player.PlayerQuitEvent;
import org.teamseas.minecraft.bossbar.BossbarService.Information;

/**
 * Displays the TeamSeas goal progress on screen.
 *
 * @link https://papermc.io/javadocs/paper/1.16/org/bukkit/boss/BossBar.html
 */
public class Bossbar {
  
  private final BossBar bar = Bukkit.createBossBar("Loading...", BarColor.BLUE, BarStyle.SOLID);

  private final BossbarRepository repository;
  private final BossbarService service;

  private final Listener listener;
  private final Runnable timerTask;

  /**
   * Constructor for Bossbar.
   */
  public Bossbar(BossbarRepository repository, BossbarService service) {
    this.repository = repository;
    this.service = service;

    listener = new BossbarListener();
    timerTask = new BossbarTimerTask();
  }

  public void setVisible(Player player) {
    bar.addPlayer(player);
  }

  public void setInvisible(Player player) {
    bar.removePlayer(player);
  }

  public Listener getListener() {
    return listener;
  }

  public Runnable getTimerTask() {
    return timerTask;
  }

  private class BossbarListener implements Listener {
    /**
     * Adds player to the bossbar when player joins.
     */
    @EventHandler
    public void onPlayerJoin(PlayerJoinEvent event) {
      Player player = event.getPlayer();

      if (!player.hasPlayedBefore()) {
        repository.setVisible(player, true);
      }
      
      if (repository.isVisible(player)) {
        setVisible(player);
      }
    }

    /**
     * Removes player from bossbar when player quits.
     */
    @EventHandler
    public void onPlayerQuit(PlayerQuitEvent event) {
      Player player = event.getPlayer();
      setInvisible(player);
    }
  }

  private class BossbarTimerTask implements Runnable {

    @Override
    public void run() {
      Optional<Information> information = service.getInformation();
      if (information.isPresent()) {
        Information info = information.get();

        bar.setProgress(info.getProgress());
        bar.setTitle(info.getText());
      }
    }
  }
}
