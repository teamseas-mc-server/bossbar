package org.teamseas.minecraft.bossbar;

import org.bukkit.OfflinePlayer;
import redis.clients.jedis.Jedis;

/**
 * Redis implementation of bossbar configuration repository.
 */
public class JedisBossbarRepository implements BossbarRepository {

  private final Jedis jedis;
  private static final String BOSSBAR_KEY = "BOSSBAR:VISIBLE";

  public JedisBossbarRepository(Jedis jedis) {
    this.jedis = jedis;
  }

  @Override
  public void setVisible(OfflinePlayer player, boolean isVisible) {
    jedis.hset(BOSSBAR_KEY, hashKey(player), Boolean.toString(isVisible));
  }

  @Override
  public boolean isVisible(OfflinePlayer player) {
    return Boolean.parseBoolean(jedis.hget(BOSSBAR_KEY, hashKey(player)));
  }

  private static String hashKey(OfflinePlayer player) {
    return player.getUniqueId().toString();
  }
}
