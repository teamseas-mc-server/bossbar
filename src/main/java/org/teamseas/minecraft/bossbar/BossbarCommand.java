package org.teamseas.minecraft.bossbar;

import org.bukkit.ChatColor;
import org.bukkit.command.Command;
import org.bukkit.command.CommandExecutor;
import org.bukkit.command.CommandSender;
import org.bukkit.entity.Player;

/**
 * Bossbar command which toggles visibility of a bossbar.
 */
public class BossbarCommand implements CommandExecutor {

  private final BossbarRepository repository;
  private final Bossbar bossbar;

  /**
   * Creates the "bossbar" command executor.
   */
  public BossbarCommand(BossbarRepository repository, Bossbar bossbar) {
    this.repository = repository;
    this.bossbar = bossbar;
  }

  /**
   * Called on /bossbar command.
   */
  @Override
  public boolean onCommand(CommandSender sender, Command command, String label, String[] args) {
    Player player = (Player) sender;

    if (args.length == 0) {
      if (repository.isVisible(player)) {
        hide(player);
      } else {
        show(player);
      }

      return true;
    }

    switch (args[0]) {
      case "on":
      case "show":
        show(player);
        break; 
      case "off":
      case "hide":
        hide(player);
        break;
      default:
        break;
    }
    
    return true;
  }

  private void show(Player player) {
    bossbar.setVisible(player);
    repository.setVisible(player, true);
    sendVisibilityMessage(player, "visible");
  }

  private void hide(Player player) {
    bossbar.setInvisible(player);
    repository.setVisible(player, false);
    sendVisibilityMessage(player, "invisible");
  }

  private static void sendVisibilityMessage(Player player, String visibility) {
    player.sendMessage(color("&Bossbar is now " + visibility + "."));
  }

  private static String color(String text) {
    return ChatColor.translateAlternateColorCodes('&', text);
  }
}
