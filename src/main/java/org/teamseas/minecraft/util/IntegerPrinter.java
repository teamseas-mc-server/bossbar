package org.teamseas.minecraft.util;

import java.text.DecimalFormat;
import java.text.DecimalFormatSymbols;
import java.util.Locale;

/**
 * Utility class to convert to unit formatted string.
 */
public class IntegerPrinter {

  /**
   * Converts an integer to a unit formatted string.
   */
  public String print(Integer value, Locale locale) {
    DecimalFormatSymbols symbols = new DecimalFormatSymbols(locale);
    DecimalFormat formatter = new DecimalFormat("#.#", symbols);
    return print((float) value, formatter);
  }

  private static String print(float value, DecimalFormat formatter) {
    String[] unit = {"", "K", "M", "B"};
    int index = 0;
    while ((value / 1000) >= 1) {
      value /= 1000;
      index++;
    }
    return String.format("%s%s", formatter.format(value), unit[index]);
  }
}
